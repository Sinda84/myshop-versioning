/**
 * 
 */
package com.ci.myshop.model;

/**
 * @author sinda
 *
 */
public class Item {

	/**
	 * @param args
	 */
	
	String name;
	int id;
	float price;
	int nbrElt;
	
	public Item(String name, int id, float price, int nbrElt) {
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}
	
	
	public String getName() {
		return name;
	}
	public int getId() {
	return id;
	}
	
	public float getPrice() {
		
		return price;
				}
	public int getNbrElt( ) {
		return nbrElt;
	}
	
	public void setName() {
			}
	public void setId () {
	}
	 public void setPrice() {
	 }
	 public void setNbreElt() {
	 }
		
	String display() {
		return this.name + this.id + this.price + this.nbrElt;
				
	}
}
