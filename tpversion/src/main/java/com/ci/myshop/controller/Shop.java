/**
 * 
 */
package com.ci.myshop.controller;

import com.ci.myshop.model.Item;

/**
 * @author sinda
 *
 */
public class Shop {

	/**
	 * @param args
	 */
	
	public Storage storage;
	float cash;
	
	public Shop () {
		this.cash = 200;
		this.storage = new Storage();
	}
	
  public Item Sell (Item item) {
	  Item i = (this.storage).itemMap.remove(item.getName());
	  this.cash += i.getNbrElt() * i.getPrice();
	  return i;
  }
  
  public float getCash() {
	  return this.cash;
  }
  
}
